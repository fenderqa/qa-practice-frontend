exports.config = {

    capabilities: {
        'browserName': 'chrome',
        'chromeOptions':  {
            'args': ['disable-infobars=true']
        }
    },
    
    directConnect: true,

    jasmineNodeOpts: {
        showColors: true
    },

    onPrepare: function () {
        var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'summary',      // display stacktrace for each failed assertion, values: (all|specs|summary|none)
            displaySuccessesSummary: false, // display summary of all successes after execution
            displayFailuresSummary: false,   // display summary of all failures after execution
            displayPendingSummary: false,    // display summary of all pending specs after execution
            displaySuccessfulSpec: true,    // display each successful spec
            displayFailedSpec: true,        // display each failed spec
            displayPendingSpec: true,      // display each pending spec
            displaySpecDuration: true,     // display each spec duration
            displaySuiteNumber: true      // display each suite number (hierarchical)
        }));
    }

};
